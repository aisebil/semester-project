import { Component } from '@angular/core';
import { Board } from './game/board';
import { Cell } from './game/cell';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent 
{
  title = 'minesweeper';
  board: Board;
  numHint: number;
  hintStop: Boolean;
  time: number;
  interval;

  constructor() 
  {}

  checkCell(cell: Cell) 
  {
    const result = this.board.checkCell(cell);
    if (result === 'gameover') 
    {
      this.pauseTimer();
      alert('You lose!');
    } 
    else if (result === 'win') 
    {
      this.pauseTimer();
      alert('You win!\nThe time you use is: '+this.time);
    }
  }
  flag(cell: Cell) 
  {
    if (cell.status === 'flag') {
      cell.status = 'open';
    } else {
      cell.status = 'flag';
    }
  }
  newGame(level:number) 
  {
    if(level===1)
    {
      this.board = new Board(8,14);
      this.numHint = 0;
    }
    else if(level===2)
    {
      this.board = new Board(14,44);
      this.numHint = 1;
    }
    else if(level===3)
    {
      this.board = new Board(20,100);
      this.numHint = 2;
    }
    this.startTimer();
  }
  hint()
  {
    this.hintStop = false;
    if(this.numHint>0)
    {
      for (let i = 0; i < this.board.cells.length; i++) {
        for (let j = 0; j < this.board.cells.length; j++) {
          if(this.board.cells[i][j].proximityMines===0&&!this.board.cells[i][j].mine&&this.board.cells[i][j].status==='open')
          {
            this.board.cells[i][j].status='clear';
            this.hintStop=true;
            break;
          }
        }
        if(this.hintStop)
          break;
      }
      this.numHint--;
    }
    else
    {
      alert('You have no hint chance.');
    }
  }
  startTimer() 
  {
    this.time = 0;
    this.interval = setInterval(() => {
      this.time++;
    },1000);
  }

  pauseTimer() {
    clearInterval(this.interval);
  }
}
